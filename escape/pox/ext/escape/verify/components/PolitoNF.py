'''
Created on Mar 5, 2015

@author: Matteo Virgilio
'''
from . import NetworkObject
import z3

class PolitoNF (NetworkObject):
    '''
    First example of custom network function: a simple filter
    '''

    def _init(self, node, network, context):
        '''
        Constructor
        '''
        self.constraints = list ()
        self.ctx = context
        self.politoNF = node.z3Node
        #network.SaneSend (self)
        
    def politoNFRules (self, ipA, ipB):
        print "[PolitoNF] Installing rules"
        n_0 = z3.Const('politoNF_%s_n_0'%(self.politoNF), self.ctx.node)
        n_1 = z3.Const('politoNF_%s_n_1'%(self.politoNF), self.ctx.node)
        t_0 = z3.Int('politoNF_%s_t_0'%(self.politoNF))
        t_1 = z3.Int('politoNF_%s_t_1'%(self.politoNF))
        p_0 = z3.Const('politoNF_%s_p_0'%(self.politoNF), self.ctx.packet)
        self.myFunction = z3.Function('%s_myFunction'%(self.politoNF), self.ctx.address, self.ctx.address, z3.BoolSort())        
        
        a_0 = z3.Const('%s_politoNF_a_0'%(self.politoNF), self.ctx.address)
        a_1 = z3.Const('%s_politoNF_a_1'%(self.politoNF), self.ctx.address)
        myConstraint = z3.ForAll([n_0, p_0, t_0], \
                            z3.Implies(self.ctx.send(self.politoNF, n_0, p_0, t_0), \
                                z3.Exists([n_1, t_1], \
                                    z3.And(self.ctx.recv(n_1, self.politoNF, p_0, t_1), \
                                           t_1 < t_0, \
                                           self.myFunction(self.ctx.packet.src(p_0), self.ctx.packet.dest(p_0))))))
        
        funcConstraint = z3.Or(z3.And(a_0 == ipA, a_1 == ipB), z3.And(a_0 == ipB, a_1 == ipA))
        self.constraints.append(z3.ForAll([a_0, a_1], self.myFunction(a_0, a_1) == funcConstraint))
        self.constraints.append(myConstraint)
        
    def _addConstraints(self, solver):
        solver.add(self.constraints)
        
    @property
    def z3Node (self):
        return self.politoNF
    
    @property
    def isEndHost (self):
        return False