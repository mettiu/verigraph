from . import NetworkObject
import z3
class PolitoWebClient (NetworkObject):
    """WebClient """
    
    def _init (self, node, network, context, ipServer):
        self.constraints = list ()
        self.ctx = context
        self.PolitoWebClient = node.z3Node
        self._webClientRules(ipServer)
        #network.SaneSend (self)

    @property
    def z3Node (self):
        return self.PolitoWebClient

    def _addConstraints (self, solver):
        print "[WebClient] Installing rules."
        solver.add(self.constraints)

    def _webClientRules (self, ipServer):
        n_0 = z3.Const('PolitoWebClient_%s_n_0'%(self.PolitoWebClient), self.ctx.node)
        t_0 = z3.Int('PolitoWebClient_%s_t_0'%(self.PolitoWebClient))
        p_0 = z3.Const('PolitoWebClient_%s_p_0'%(self.PolitoWebClient), self.ctx.packet)
        #self.constraints.append(1 == 2)
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoWebClient, n_0, p_0, t_0), \
                self.ctx.nodeHasAddr(self.PolitoWebClient, self.ctx.packet.src(p_0)))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoWebClient, n_0, p_0, t_0), \
                self.ctx.packet.origin(p_0) == self.PolitoWebClient)))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoWebClient, n_0, p_0, t_0), \
                self.ctx.packet.orig_body(p_0) == self.ctx.packet.body(p_0))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.recv(n_0, self.PolitoWebClient, p_0, t_0), \
                self.ctx.nodeHasAddr(self.PolitoWebClient, self.ctx.packet.dest(p_0)))))
        
        # This client is only able to produce HTTP requests
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoWebClient, n_0, p_0, t_0), \
                           self.ctx.packet.proto(p_0) == self.ctx.HTTP_REQUEST)))
        
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoWebClient, n_0, p_0, t_0), \
                           self.ctx.packet.dest(p_0) == ipServer)))

    @property
    def isEndHost (self):
        return True