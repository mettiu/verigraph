'''
Created on 09/mar/2015

@author: Serena
'''
from . import NetworkObject, Core
import z3

class PolitoNat (NetworkObject):
    def _init(self, node, network, context):
        self.constraints = list()
        self.nat = node.z3Node
        self.ctx = context
        self.private_addresses = []
        self.private_node = []
        network.SaneSend(self)
        self.natModel()
        
    def _addConstraints (self, solver):
        solver.add(self.constraints)
        
    def _addPrivateAdd (self, address):
        if address is not isinstance(address, list):
            address = [address]
        self.private_addresses.extend(address) 
        

    @property
    def PrivateAddress(self):
        return self.private_addresses
 
    @property    
    def z3Node (self):
        return self.nat
    
    def natModel(self):
        x = z3.Const('x', self.ctx.node)
        y = z3.Const('y', self.ctx.node)
        z = z3.Const('z', self.ctx.node)
        
        p_0 = z3.Const('p_0', self.ctx.packet)
        p_1 = z3.Const('p_1', self.ctx.packet)
        p_2 = z3.Const('p_2', self.ctx.packet)
        
        t_0 = z3.Int('t_0')
        t_1 = z3.Int('t_1')
        t_2 = z3.Int('t_2')

        self.private_addr_func = z3.Function('private_addr_func', self.ctx.address, z3.BoolSort()) 
             
          
        self.constraints.append(z3.ForAll([t_0, p_0, x], \
                                    z3.Implies( \
                                            z3.And(self.ctx.send(self.nat, x, p_0, t_0), \
                                                   z3.Not(self.private_addr_func(self.ctx.packet.dest(p_0))),\
                                                   ),\
                                               z3.And(\
                                                    self.ctx.packet.src(p_0) == self.ctx.ip_nat,\
                                                    z3.Exists([y, p_1, t_1],\
                                                                z3.And(self.ctx.recv(y, self.nat, p_1, t_1),\
                                                                       t_1 < t_0,\
                                                                       self.private_addr_func(self.ctx.packet.src(p_1)),\
                                                                       self.ctx.packet.origin(p_1) == self.ctx.packet.origin(p_0),\
                                                                       self.ctx.packet.dest(p_1) == self.ctx.packet.dest(p_0),\
                                                                       self.ctx.packet.orig_body(p_1) == self.ctx.packet.orig_body(p_0),\
                                                                       self.ctx.packet.body(p_1) == self.ctx.packet.body(p_0),\
                                                                       self.ctx.packet.seq(p_1) == self.ctx.packet.seq(p_0),\
                                                                       self.ctx.packet.proto(p_1) == self.ctx.packet.proto(p_0),\
                                                                       self.ctx.packet.emailFrom(p_1) == self.ctx.packet.emailFrom(p_0),\
                                                                       self.ctx.packet.url(p_1) == self.ctx.packet.url(p_0),\
                                                                       self.ctx.packet.options(p_1) == self.ctx.packet.options(p_0),\
                                                                       )\
                                                                )\
                                                      )\
                                               )\
                                          )\
                                )
        
        self.constraints.append(z3.ForAll([x, p_0, t_0], \
                                         z3.Implies( \
                                                     z3.And(self.ctx.send(self.nat, x, p_0, t_0),  
                                                            self.private_addr_func(self.ctx.packet.dest(p_0))\
                                                            ),\
                                                     z3.And(
                                                            z3.Not(self.private_addr_func(self.ctx.packet.src(p_0))),\
                                                            z3.Exists([y, p_1, t_1], \
                                                                        z3.And(t_1 < t_0, \
                                                                               self.ctx.recv(y, self.nat, p_1, t_1), \
                                                                               z3.Not(self.private_addr_func(self.ctx.packet.src(p_1))), \
                                                                               self.ctx.packet.dest(p_1) == self.ctx.ip_nat, \
                                                                               self.ctx.packet.src(p_1) == self.ctx.packet.src(p_0),\
                                                                               self.ctx.packet.origin(p_0) == self.ctx.packet.origin(p_1),\
                                                                               self.ctx.packet.orig_body(p_1) == self.ctx.packet.orig_body(p_0),\
                                                                               self.ctx.packet.body(p_1) == self.ctx.packet.body(p_0),\
                                                                               self.ctx.packet.seq(p_1) == self.ctx.packet.seq(p_0),\
                                                                               self.ctx.packet.proto(p_1) == self.ctx.packet.proto(p_0),\
                                                                               self.ctx.packet.emailFrom(p_1) == self.ctx.packet.emailFrom(p_0),\
                                                                               self.ctx.packet.url(p_1) == self.ctx.packet.url(p_0),\
                                                                               self.ctx.packet.options(p_1) == self.ctx.packet.options(p_0),\
                                                                               z3.Exists([z, p_2, t_2],\
                                                                                         z3.And(t_2 < t_1, \
                                                                                                self.ctx.recv(z, self.nat, p_2, t_2), \
                                                                                                self.private_addr_func(self.ctx.packet.src(p_2)), \
                                                                                                self.ctx.packet.src(p_1) == self.ctx.packet.dest(p_2), \
                                                                                                self.ctx.packet.src(p_0) == self.ctx.packet.dest(p_2),\
                                                                                                self.ctx.packet.src(p_2) ==  self.ctx.packet.dest(p_0)
                                                                                                ))\
                                                                               )\
                                                      ))\
                                                    )\
                                          )\
                                )
        
        
    def setInternalAddress(self, internalAddress):
        constraints = []
        n_0 = z3.Const('nat_node', self.ctx.address)
        for n in internalAddress:
            constraints.append(n_0 == n)
            
        self.constraints.append(z3.ForAll([n_0], self.private_addr_func(n_0) == z3.Or(constraints)))       