from . import NetworkObject
import z3
class PolitoMailServer (NetworkObject):
    """Mail server objects"""
    def _init (self, node, network, context):
        self.constraints = list ()
        self.ctx = context
        self.PolitoMailServer = node.z3Node
        self._mailServerRules()
        network.SaneSend (self)

    @property
    def z3Node (self):
        return self.PolitoMailServer

    def _addConstraints (self, solver):
        solver.add(self.constraints)

    def _mailServerRules (self):
        n_0 = z3.Const('%s_n_0'%(self.PolitoMailServer), self.ctx.node)
        t_0 = z3.Int('%s_t_0'%(self.PolitoMailServer))
        t_1 = z3.Int('%s_t_1'%(self.PolitoMailServer))
        p_0 = z3.Const('%s_p_0'%(self.PolitoMailServer), self.ctx.packet)
        p_1 = z3.Const('%s_p_1'%(self.PolitoMailServer), self.ctx.packet)
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailServer, n_0, p_0, t_0), \
                self.ctx.nodeHasAddr(self.PolitoMailServer, self.ctx.packet.src(p_0)))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailServer, n_0, p_0, t_0), \
                self.ctx.packet.origin(p_0) == self.PolitoMailServer)))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailServer, n_0, p_0, t_0), \
                self.ctx.packet.orig_body(p_0) == self.ctx.packet.body(p_0))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.recv(n_0, self.PolitoMailServer, p_0, t_0), \
                self.ctx.nodeHasAddr(self.PolitoMailServer, self.ctx.packet.dest(p_0)))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailServer, n_0, p_0, t_0), \
                z3.And(self.ctx.packet.proto(p_0) == self.ctx.POP3_RESPONSE, self.ctx.packet.emailFrom(p_0) == 1))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], 
                                    z3.Implies(self.ctx.send(self.PolitoMailServer, n_0, p_0, t_0), 
                                        z3.Exists([p_1, t_1], 
                                            z3.And(t_1 < t_0,
                                                   self.ctx.recv(n_0, self.PolitoMailServer, p_1, t_1),
                                                   self.ctx.packet.proto(p_0) == self.ctx.POP3_RESPONSE,
                                                   self.ctx.packet.proto(p_1) == self.ctx.POP3_REQUEST,
                                                   self.ctx.packet.dest(p_0) == self.ctx.packet.src(p_1))))))
#         self.constraints.append(z3.ForAll([n_0, p_0, t_0], 
#                                     z3.Implies(self.ctx.send(self.PolitoMailServer, n_0, p_0, t_0),
#                                                self.ctx.packet.emailFrom(p_0) == 2)));
