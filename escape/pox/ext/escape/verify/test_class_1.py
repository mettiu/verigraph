import components
def test_class_1():
    ctx = components.Context(['firewall', 'mailclient', 'antispam', 'mailserver', 'nat'],['ip_firewall', 'ip_mailclient', 'ip_antispam', 'ip_mailserver', 'ip_nat'])
    net = components.Network (ctx)
    firewall = components.AclFirewall(ctx.firewall, net, ctx)
    mailclient = components.EndHost(ctx.mailclient, net, ctx)
    antispam = components.PolitoAntispam(ctx.antispam, net, ctx)
    mailserver = components.EndHost(ctx.mailserver, net, ctx)
    nat = components.PolitoNat(ctx.nat, net, ctx)
    net.setAddressMappings([(firewall, ctx.ip_firewall), (mailclient, ctx.ip_mailclient), (antispam, ctx.ip_antispam), (mailserver, ctx.ip_mailserver), (nat, ctx.ip_nat)])
    #configure routing table
    net.RoutingTable(firewall, [(ctx.ip_antispam, nat), (ctx.ip_mailclient, nat), (ctx.ip_nat, nat), (ctx.ip_mailserver, mailserver)])
    net.RoutingTable(mailclient, [(ctx.ip_firewall, antispam), (ctx.ip_antispam, antispam), (ctx.ip_nat, antispam), (ctx.ip_mailserver, antispam)])
    net.RoutingTable(antispam, [(ctx.ip_firewall, nat), (ctx.ip_mailclient, mailclient), (ctx.ip_nat, nat), (ctx.ip_mailserver, nat)])
    net.RoutingTable(mailserver, [(ctx.ip_firewall, firewall), (ctx.ip_antispam, firewall), (ctx.ip_mailclient, firewall), (ctx.ip_nat, firewall)])
    net.RoutingTable(nat, [(ctx.ip_firewall, firewall), (ctx.ip_antispam, antispam), (ctx.ip_mailclient, antispam), (ctx.ip_mailserver, firewall)])
    #attach devices
    net.Attach(firewall, mailclient, antispam, mailserver, nat)
    #configure middle-boxes
    nat.setInternalAddress([ctx.ip_mailclient, ctx.ip_antispam])
    class AutoGeneratedReturn1 (object):
        def __init__ (self, net, ctx, firewall, mailclient, antispam, mailserver, nat):
            self.net = net
            self.ctx = ctx
            self.firewall = firewall
            self.mailclient = mailclient
            self.antispam = antispam
            self.mailserver = mailserver
            self.nat = nat
            self.check = components.PropertyChecker (ctx, net)
    return AutoGeneratedReturn1(net, ctx, firewall, mailclient, antispam, mailserver, nat)

