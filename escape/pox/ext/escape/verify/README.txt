VERIFY_STANDALONE.PY

This modules allows you to perfom a reachability verification between two nodes of a given graph.
This verification process consists into a request to a local instance of the verification web service (available here: https://gitlab.com/mettiu/rest-verigraph/blob/master/dist/verify.war), which submits a given graph (described in JSON format) to another locally deployed web service (Neo4jManager, available here: https://gitlab.com/mettiu/rest-verigraph/blob/master/dist/neo4jmanager.war) in order to extract all the possible linear walks between a given source and destination, here called chains. For each extracted chain a scenario file is generated in addition to the corresponding test module, which is then tested against Z3 (theorem prover from Microsoft Research). The result of the reachability test is then returned to the user and can be either "SAT" or "UNSAT", depending on whether the reachability constraint is satisfied or unsatisfied. For statistical purposes the test is repeated multiple times and the mean time elapsed is printed to the standard output.

In order to execute the standalone verification a JSON file describing the input graph is required and the choice for this purpose is to adopt the same structure used by Escape (you can read more about this on their online documentation, which can be found here: https://sb.tmit.bme.hu/escape/util/nffg_elements.html). In any case you can find several examples of input graphs at the following link: https://gitlab.com/mettiu/verigraph/tree/master/escape/examples.

According to Escape devices are distinguished into three categories:
saps -> "node_saps" : these are in essence endpoints (source and destination nodes must belong to this type)
infras -> "node_infras" : these are the configurable elements, i.e. those that can implement a VNF (Virtual Network Function), set into the "supported" field (note that the verification process parses only the first supported type, which can be chosen amongst the ones given below)
links -> "edge_links" : a connection between two nodes has to be represented in this form

Other than the fields above, for compatibility with Escape, i.e. in case you want to launch and test your example with Escape, you have to add two additional fields: "edge_sg_nexthops" and "edge_reqs". Please refer to the Escape documentation for further information.

Keep in mind that the currently supported types for VNFs are:
"webclient", "webserver", "cache","nat", "firewall", "mailclient",
"mailserver", "antispam", "endpoint"
All the Python modules that are needed can be downloaded here: https://gitlab.com/mettiu/verigraph/tree/master/escape/pox/ext/escape/verify
Other than that Z3 for Python has to be installed on your machine and the previously mentioned web services have to be running locally on port 8080 of the latest version of Tomcat 8.
Before running the verify_standalone.py module the verification has to be configured and this is done by editing the "verification_config.json" and "policies.json" files. The latter allows you to set source and destination nodes ("source" and "destination" fields), the reachability policy ("policy" field) and the file describing the input graph ("nffg_file" field. Please place this file in the default directory of Escape, i.e. under '/home/mininet/escape/examples'). The former allows you to set the configuration file ("configuration_file" field) and the policies file ("policies_file" field).
The configuration file is a static file which describes the configuration of all the nodes contained in the graph.
A sample config file is pasted below:

{
	"nodes":[
		{
			"configuration":[
				
			],
			"id":"endpoint",
			"description":"endpoint"
		},
		{
			"configuration":[
				"ip_user1",
				"ip_user2"
			],
			"id":"nat",
			"description":"internal addresses to be natted"
		},
		{
			"configuration":[
				{
					"ip_server":"ip_client"
				}
			],
			"id":"firewall",
			"description":"BLACKLIST: ip_destination:ip_source"
		},
		{
			"configuration":[

			],
			"webserver":"ip_webserver"
			"id":"webclient",
			"description":"web client"
		},
		{
			"configuration":[
				
			],
			"id":"webserver",
			"description":"web server"
		},
		{
			"configuration":[
				"ip_user1",
				"ip_user2"
			],
			"id":"cache",
			"description":"Known addresses"
		},
		{
			"configuration":[

			],
			"mailserver":"ip_mailserver"
			"id":"mailclient",
			"description":"mail client"
		},
		{
			"configuration":[

			],
			"id":"mailserver",
			"description":"mail server"
		},
		{
			"configuration":[
				"ip_mailclient"
			],
			"id":"antispam",
			"description":"antispam"
		}
	]
}

Please note that "webclient" and "mailclient" nodes, other than "id" and "configuration" fields, need a dedicated field ("webserver" and "mailserver") respectively to store the IP address of the corresponding server.

Once everything is set the verification can be launched executing the "verify_standalone.py" file according to the following example:

verify_standalone.py -l <loglevel> -f <logfile>

where both the parameters are optional:

loglevel: warning, info, debug (info if not set)
logfile: name of the log file (stdout if not set)


VERIFICATION THROUGH ESCAPE

These are the required steps:
- launch tomcat and deply verify.war and neo4jmanager.war (see instructions above)
- configure the "verification_config.json" and "policies.json" files according to the instuctions above.
- execute the following command:
cd /home/mininet
(please set <example_name> in the following command to the name of the desired nffg file)
./escape.py --config pox/ext/escape/config.py -dfi -s examples/<example_name>.nffg

The pre-mapping verification is triggered automatically (it is enabled by default in the custom "config.py" file) and a ProcessorError is thrown in case the reachability constraint is unsatisfied.  
