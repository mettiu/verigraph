import z3
import time
import json
import sys
from test_class_1 import test_class_1
def ResetZ3():
    z3._main_ctx = None
    z3.main_ctx()
def PrintVector (array):
    i=0
    print "*** Printing vector ***"
    for a in array:
        i+=1
        print "#",i
        print a
    print "*** ", i, " elements printed! ***"
def PrintModel (model):
    for d in model.decls():
        print "%s = %s" % (d.name(), model[d])
        print ""
def main():
    k=0
    t=0
    for i in range(1,2):
        ResetZ3()
        model = test_class_1()
        start_time = time.time()
        ret = model.check.CheckIsolationProperty(model.mailclient, model.mailserver)
        t = t+(time.time() - start_time)
        k=k+1
        #PrintVector(ret.assertions)
        if ret.result == z3.unsat:
            result = 0
            #print("UNSAT")
        else:
            result = 1
            #print("SAT")
    #print "Mean execution time hostA -> hostB", t/k
    sys.stdout.write(str(result))
if __name__ == "__main__":
    main()

