import sys,os,os.path
import json
from pprint import pprint
from pprint import pformat
import re
import requests
import subprocess
from lib import test_class_generator
from lib import test_generator
from lib import config
from lib import utility
import logging


class Premapper():
    
    def __init__(self, input_graph):
        self.get_verification_configuration()
        self.get_policies(filename = "/home/mininet/escape/pox/ext/escape/verify/" + self.policies_file)
        # initialize paths
        self.paths = []
        # initialize nodes
        self.nodes = self.get_nodes(input_graph)


    def get_verification_configuration(self, filename = "/home/mininet/escape/pox/ext/escape/verify/verification_config.json"):
        with open(filename, 'r') as f:
            verification_config = json.load(f)
        self.configuration_file = verification_config["configuration_file"]
        self.policies_file = verification_config["policies_file"]

    def get_nodes(self, input_graph):
        #initialize nodes
        nodes = []
        # loop to set nodes

        # i = 0
        # for node, node_data in input_graph.__iter__(data=True):
        #     nodes.insert(i, {})
        #     nodes[i]["name"] = node
        #     nodes[i]["functional_type"] = self.get_functional_type(node)
        #     nodes[i]["neighbours"] = []    
        #     i = i+1

        i=0
        for sap in input_graph.saps:
            nodes.insert(i, {})
            nodes[i]["name"] = sap.id
            nodes[i]["functional_type"] = "endpoint"
            nodes[i]["neighbours"] = []    
            i = i+1

        for infra in input_graph.infras:
            nodes.insert(i, {})
            nodes[i]["name"] = infra.name
            nodes[i]["functional_type"] = infra.supported[0]
            if nodes[i]["functional_type"] == "dpi":
                nodes[i]["functional_type"] = "nat"
            nodes[i]["neighbours"] = []    
            i = i+1

        #loop to set neighbours

        #set nodes based on edge links
        # for edge in input_graph.sg_hops:                      
        #     source = edge.src.node.id
        #     destination = edge.dst.node.id
        #     for i in range(0, len(nodes)):
        #         if nodes[i]["name"] == source:
        #             nodes[i]["neighbours"].append(destination)
        #             break

        #set nodes based on edge next hops
        for link in input_graph.links:
            source = link.src.node.id
            destination = link.dst.node.id
            for i in range(0, len(nodes)):
                if nodes[i]["name"] == source:
                    nodes[i]["neighbours"].append(destination)
                    break

        logging.debug(pformat(nodes))
        return nodes

    def get_functional_type(self, name):
        filename = "/home/mininet/escape/examples/" + self.nffg_file
        with open(filename, 'r') as f:
            nffg = json.load(f)
        for node in nffg["node_nfs"]:
            if node["id"] == name:
                return node["functional_type"]
        for node in nffg["node_infras"]:
            if node["id"] == name:
                return node["supported"][0]
        return "endpoint"

    def get_policies(self, filename = "/home/mininet/escape/pox/ext/escape/verify/policies.json"):
        with open(filename, 'r') as f:
            policies = json.load(f)
        self.source = policies["source"]
        self.destination = policies["destination"]
        self.policy = policies["policy"]
        self.nffg_file = policies["nffg_file"]

    def add_nodes(self, ip = "127.0.0.1"):
        # set headers
        headers = {'Content-type': 'application/json'}
        # set url to add nodes 
        nodes_url = "http://" + ip + ":8080/verify/api/nodes"
        for node in self.nodes:
            logging.debug("Adding node \"" + node["name"] + "\"...")
            # set payload for post request
            data_json = json.dumps(node)
            # perform post request    
            response = requests.post(nodes_url, data = data_json, headers = headers)
            # print response
            logging.debug(pformat(response.json()))
            # get node_id from response
            node_id = response.json()["id"]

            logging.debug("Node \"" + node["name"] + "\" has been added successfully!")    
            logging.debug("Adding neighbours of node \"" + node["name"] + "\"...")
            # loop to add neighbours
            for neighbour in node["neighbours"]:
                # initialize and set payload
                data = {}
                data["name"] = neighbour
                data_json = json.dumps(data)
                # set url for post request
                node_neighbours_url = nodes_url + "/" + str(node_id) + "/neighbours"
                # perform post request
                response = requests.post(node_neighbours_url, data = data_json, headers = headers)    
                # print response
                logging.debug(pformat(response.json()))

                logging.debug("Neighbour \"" + neighbour + "\" of node \"" + node["name"] + "\" has been added successfully!")

    def get_chains(self, ip = "127.0.0.1"):
        # set url for get request
        chains_url = "http://" + ip + ":8080/verify/api/chains?source=" + self.source + "&destination=" + self.destination 
        # set headers
        headers = {'Content-type': 'application/json'}
        # perform get request
        response = requests.get(chains_url, headers = headers)
        # print response
        logging.debug(pformat(response.json()))
        # loop to parse response
        for path in response.json()["path"]:
            # parse nodes between parentheses
            chain = re.findall('\((.+?)\)', path)
            # prepare regular expression to esclude connection ports
            regex = re.compile(r'\b.*(_in|_out)\b')
            # filter connection ports
            filtered_chain = [i for i in chain if not regex.search(i)]
            # print filtered strings
            logging.debug(" ".join(filtered_chain))
            # filter node id after underscore
            filtered_chain = [each.rsplit('_',1)[0] for each in filtered_chain]
            # print filtered results
            logging.debug(" ".join(filtered_chain))
            # add chain to paths
            self.paths.append(filtered_chain)

    def generate_chains_for_tests(self):
        # initialize chains
        self.chains = {}
        self.chains["chains"] = []
        # set filename
        self.chains_filename = "/home/mininet/escape/pox/ext/escape/verify/chains.json"
        
        number_of_chains = len(self.paths)
        for i in range(0, number_of_chains):
            self.chains["chains"].insert(i, {})
            self.chains["chains"][i]["id"] = i+1
            flowspace = "tcp=80"
            self.chains["chains"][i]["flowspace"] = flowspace
            chain_nodes = len(self.paths[i])
            self.chains["chains"][i]["nodes"] = []
            for j in range(0, chain_nodes):
                self.chains["chains"][i]["nodes"].insert(j, {})
                node_name = self.paths[i][j]
                self.chains["chains"][i]["nodes"][j]["name"] = node_name
                node_type = next((node["functional_type"] for node in self.nodes if node["name"] == node_name), [None])
                self.chains["chains"][i]["nodes"][j]["functional_type"] = node_type
                node_address = "ip_" + node_name
                self.chains["chains"][i]["nodes"][j]["address"] = node_address
        logging.debug(pformat(self.chains))

        with open(self.chains_filename, 'w') as f:
            print >>f, json.dumps(self.chains) 

    def generate_tests(self):
        # generate test classes
        for i in range(0, len(self.chains["chains"])):
            arguments = ["-c", self.chains_filename, "-f", "/home/mininet/escape/pox/ext/escape/verify/" + self.configuration_file, "-o", "/home/mininet/escape/pox/ext/escape/verify/test_class.py"]
            test_class_generator.main(arguments)
        # generate test scenarios
        self.scenarios = []
        for i in range(0, len(self.chains["chains"])):
            self.scenarios.append("/home/mininet/escape/pox/ext/escape/verify/test_scenario_" + str(i+1) + ".py")
            arguments = ["-i", "/home/mininet/escape/pox/ext/escape/verify/test_class_" + str(i+1) + ".py", "-o", "/home/mininet/escape/pox/ext/escape/verify/test_scenario_" + str(i+1) + ".py", "-s", self.source, "-d", self.destination]
            test_generator.main(arguments)

    def run_tests(self):
        self.results = []
        for scenario in self.scenarios:
            marshal, unmarshal = json.dumps, json.loads
            p = subprocess.Popen([sys.executable, scenario], stdout=subprocess.PIPE)
            result = p.communicate()[0]
            #result = subprocess.check_output(["python", scenario])
            self.results.append(result)
        logging.debug(pformat(self.results))
        final_result = False
        for result in self.results:
            if result == "1" :
                final_result = True
                break
        return final_result

