import sys,os,os.path
#useful for import mapping
sys.path.append(os.path.expanduser('~/escape-shared/pox/ext/escape/util'))
#useful for from escape import *
sys.path.append(os.path.expanduser('~/escape-shared/pox/ext/'))
#useful for from pox import *
sys.path.append(os.path.expanduser('~/escape-shared/pox'))
#useful for from pox import Catalog
sys.path.append(os.path.expanduser('~/escape-shared/mininet'))
#useful for from pox import premapper
sys.path.append(os.path.expanduser('~/escape-shared/pox/ext/escape/verify'))
from pox.core import core
import mapping
from pprint import pprint
import requests
import json
import re
from premapper import Premapper
from escape.util.mapping import ProcessorError
import time
from datetime import datetime
from lib import utility

class GraphVerify(mapping.AbstractMappingDataProcessor):
    
    def pre_mapping_exec (self, input_graph, resource_graph):
        with utility.smart_open("log.txt") as f:
            t0 = time.clock()
            print datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            
            core.getLogger().info("Verifying graph before mapping...")

            print >> f, "Initialize Premapper start:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            premapper = Premapper(input_graph)
            print >> f, "Initialize Premapper end:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

            core.getLogger().info("Grenerating requests for the verify web service...")

            pprint(premapper.nodes)
            
            print >> f, "Add nodes start:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            premapper.add_nodes()
            print >> f, "Add nodes end:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

            core.getLogger().info("Getting all the possible chains between source and destination nodes...")

            print >> f, "Get chains start:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            premapper.get_chains()
            print >> f, "Get chains end:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

            core.getLogger().info("Generating all possible test scenarios...")
            
            print >> f, "Generate chains for tests start:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            premapper.generate_chains_for_tests()
            print >> f, "Generate chains for tests end:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]


            core.getLogger().info("Generating all the tests...")
            print >> f, "Generate tests start:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            premapper.generate_tests()
            print >> f, "Generate tests end:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

            core.getLogger().info("Running tests...")
            print >> f, "Run tests start:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            result = premapper.run_tests()
            print >> f, "Run tests end:"
            print >> f, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            
            t = time.clock() - t0
            print datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
            
            print "Elapsed time for verification is " + str(t) + " seconds"
            
            if result == False:
                raise ProcessorError("Pre-mapping verification failed!")
                
            # all tests passed
            return False

    def post_mapping_exec (self, input_graph, resource_graph, result_graph):
        return False
