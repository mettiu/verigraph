#!/usr/bin/python
import sys,os,os.path
sys.path.append(os.path.expanduser('~/escape/pox/ext/escape/verify'))

from pprint import pprint
from code_generator import CodeGeneratorBackend
import getopt
import json
import contextlib
from utility import *
from premapper import Premapper
import logging

class SapType():
    def __init__(self, id):
        self.id = id

class InfraType():
    def __init__(self, name, supported = []):
        self.name = name
        self.supported = supported

class NodeType():
    def __init__(self, id):
        self.id = id

class PortType():
    def __init__(self, node):
        self.node = NodeType(node)

class LinkType():
    def __init__(self, src, dst):
        self.src = PortType(src)
        self.dst = PortType(dst)

class NffgType():
    def __init__(self, nffg):
        self.saps = []
        self.infras = []
        self.links = []

        for sap in nffg['node_saps']:
            self.saps.append(SapType(sap['id']))
        for infra in nffg['node_infras']:
            self.infras.append(InfraType(infra['name'], infra['supported']))
        for link in nffg['edge_links']:
            self.links.append(LinkType(link['src_node'], link['dst_node']))

class NffgParser():

    def __init__(self, config_file = "verification_config.json"):
        current_directory = os.path.dirname(os.path.abspath(__file__))
        parent_directory = os.path.split(current_directory)[0]
        config_file = os.path.join(parent_directory, config_file)

        #f = open(config_file, 'r')
        with open(config_file, 'r') as f:
            verification_config = json.load(f)
        self.configuration_file = verification_config["configuration_file"]
        self.policies_file = os.path.join(parent_directory, verification_config["policies_file"])

        #f = open(self.policies_file, 'r')
        with open(self.policies_file, 'r') as f:
            policies = json.load(f)
        self.source = policies["source"]
        self.destination = policies["destination"]
        self.policy = policies["policy"]
        self.nffg_file = policies["nffg_file"]

        current_directory = os.path.dirname(os.path.abspath(__file__))
        for i in range (0, 5):
            parent_directory = os.path.split(current_directory)[0]
            current_directory = parent_directory
        file_path = os.path.join(parent_directory, 'examples', self.nffg_file)
        #f = open(file_path, 'r')
        
        with open(file_path, 'r') as f:
            self.nffg = json.load(f)

    def input_graph(self):
        graph = NffgType(self.nffg)
        return graph
        

def main(argv):

    loglevel = 'info'
    logfile = ''

    try:
        opts, args = getopt.getopt(argv,"hl::f::",["help","log=","logfile="])
    except getopt.GetoptError as err:
        print str(err)
        print 'verify_standalone.py -l <loglevel> -f <logfile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print 'verify_standalone.py -l <loglevel> -f <logfile>' 
            sys.exit(2)
        elif opt in ("-l", "--log"):
            loglevel = arg
        elif opt in ("-f", "--logfile"):
            logfile = arg

    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)

    FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"

    if logfile != '':
        logging.basicConfig(level=numeric_level, filename=logfile, format=FORMAT)
    else:   
        logging.basicConfig(stream=sys.stderr, level=numeric_level, format=FORMAT)

    parser = NffgParser()

    premapper = Premapper(parser.input_graph())
    
    premapper.add_nodes()

    premapper.get_chains()

    premapper.generate_chains_for_tests()

    premapper.generate_tests()

    result = premapper.run_tests()

    if result == True:
        print "SAT"
    else:
        print "UNSAT"
               

if __name__ == "__main__":
    main(sys.argv[1:])
