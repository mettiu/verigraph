#!/usr/bin/python

from pprint import pprint
from code_generator import CodeGeneratorBackend
import sys, getopt
import contextlib
import os
from utility import *
import logging

def main(argv):
    if len(argv) < 8:
        print 'test_generator.py -i <inputfile> -o <outputfile> -s <source> -d <destination>'
        sys.exit(2)
    
    loglevel = "info"

    try:
        opts, args = getopt.getopt(argv,"hi:o:s:d:l",["ifile=","ofile=","source=","destination=","log="])
    except getopt.GetoptError:
        print 'test_generator.py -i <inputfile> -o <outputfile> -s <source> -d <destination> [-l <loglevel>]'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'test_generator.py -i <inputfile> -o <outputfile> -s <source> -d <destination> [-l <loglevel>]'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-s", "--source"):
            source = arg
        elif opt in ("-d", "--destination"):
            destination = arg
        elif opt in ("-l", "--log"):
            loglevel = arg

    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)

    logging.basicConfig(stream=sys.stderr, level=numeric_level)

    #print arguments    
    logging.info('Input file is ' + os.path.abspath(inputfile))
    logging.info('Output file is ' + os.path.abspath(outputfile))
    logging.info('Source node is \"' + source + "\"")
    logging.info('Destination node is \"' + destination +"\"")
    
    #begin file generation
    with smart_open(outputfile) as f:
        c = CodeGeneratorBackend()
        c.begin(tab="    ")
        
        c.writeln("import z3")
        c.writeln("import time")
        c.writeln("import json")
        c.writeln("import sys")

        c.write("from ")
        inputfile = os.path.basename(inputfile)
        c.append(os.path.splitext(inputfile)[0])
        c.append(" import ")
        c.append(os.path.splitext(inputfile)[0] + "\n")

        c.writeln("def ResetZ3():")

        c.indent()
        c.writeln("z3._main_ctx = None")

        c.writeln("z3.main_ctx()")

        c.dedent()
        c.writeln("def PrintVector (array):")

        c.indent()
        c.writeln("i=0")

        c.writeln("print \"*** Printing vector ***\"")

        c.writeln("for a in array:")

        c.indent()
        c.writeln("i+=1")

        c.writeln("print \"#\",i")
        
        c.writeln("print a")

        c.dedent()
        c.writeln("print \"*** \", i, \" elements printed! ***\"")

        c.dedent()
        c.writeln("def PrintModel (model):")

        c.indent()
        c.writeln("for d in model.decls():")

        c.indent()
        c.writeln("print \"%s = %s\" % (d.name(), model[d])")

        c.writeln("print \"\"")

        c.dedent()
        c.dedent()
        
        c.writeln("def main():")
        c.indent()

        #adding time estimation
        c.writeln("k=0")
        c.writeln("t=0")
        
        c.writeln("for i in range(1,2):")
        c.indent()
        
        c.writeln("ResetZ3()")

        c.write("model = ")
        c.append(os.path.splitext(inputfile)[0] + "()\n")
        
        c.writeln("start_time = time.time()")

        c.write("ret = model.check.CheckIsolationProperty(model.")
        c.append(source + ", model." + destination + ")\n")
        
        c.writeln("t = t+(time.time() - start_time)")
        c.writeln("k=k+1")

        c.writeln("#PrintVector(ret.assertions)")

        c.writeln("if ret.result == z3.unsat:")

        c.indent()
        c.writeln("result = 0")
        c.writeln("#print(\"UNSAT\")")

        c.dedent()
        c.writeln("else:")

        c.indent()
        c.writeln("result = 1")
        c.writeln("#print(\"SAT\")")
        c.dedent()
        
        #endfor
        c.dedent()
        c.writeln("#print \"Mean execution time hostA -> hostB\", t/k")
        c.writeln("sys.stdout.write(str(result))")

        c.dedent()

        c.writeln("if __name__ == \"__main__\":")

        c.indent()
        c.writeln("main()")

        print >>f, c.end()
    logging.info("File " + os.path.abspath(outputfile) + " has been successfully generated!")
               

if __name__ == "__main__":
    main(sys.argv[1:])
