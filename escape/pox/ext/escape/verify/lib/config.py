#!/usr/bin/python

devices_to_classes = {  "webclient" : "PolitoWebClient",
                        "webserver" : "PolitoWebServer",
                        "cache" : "PolitoCache",
                        "nat" : "PolitoNat",
                        "firewall" : "AclFirewall",
                        "mailclient" : "PolitoMailClient",
                        "mailserver" : "PolitoMailServer",
                        "antispam" : "PolitoAntispam",
                        "endpoint": "EndHost"
                     }
devices_to_configuration_methods = {"webclient" : "",
                                    "webserver" : "",
                                    "cache" : "installCache",
                                    "nat" : "setInternalAddress",
                                    "firewall" : "AddAcls",
                                    "mailclient" : "",
                                    "mailserver" : "",
                                    "antispam" : "",
                                    "endpoint": ""
                                    }
devices_initialization = {  "webclient" : ["webserver"],
                            "webserver" : [],
                            "cache" : [],
                            "nat" : [],
                            "firewall" : [],
                            "mailclient" : ["mailserver"],
                            "mailserver" : [],
                            "antispam" : [],
                            "endpoint": []                          
                          }

devices_configuration_methods = {   "webclient" : "list",
                                    "webserver" : "list",
                                    "cache" : "list",
                                    "nat" : "list",
                                    "firewall" : "maps",
                                    "mailclient" : "list",
                                    "mailserver" : "list",
                                    "antispam" : "list",
                                    "endpoint": "list" 
                                 }

devices_configuration_fields = {    "webclient" : "",
                                    "webserver" : "",
                                    "cache" : "cached address",
                                    "nat" : "natted address",
                                    "firewall" : "acl entry",
                                    "mailclient" : "",
                                    "mailserver" : "",
                                    "antispam" : "",
                                    "endpoint": "" 
                                 }
