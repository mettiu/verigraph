#!/usr/bin/python

from pprint import pprint
from pprint import pformat
import sys, getopt
from code_generator import CodeGeneratorBackend
import os, errno
from config import *
from utility import *
from routing_generator import *
import logging
    
#generates a custom test file
def generate_test_file(chain, number, configuration, output_file="test_class"):
    route = {}
    config = {}
    chn = {}

    #initiatlize the config dictionary for each node
    for node in chain["nodes"]:
        config[node["name"]] = {}

    #initiatlize the route dictionary for each node
    for node in chain["nodes"]:       
        route[node["name"]] = {}

    #initiatlize the chn dictionary for each node
    for node in chain["nodes"]:       
        chn[node["name"]] = {}

    #set chn values ---> chn(name, (field, value))
    for node in chain["nodes"]:
        for key, value in node.items():
            try:
                #name key is redundant in map
                if key != "name":
                    chn[node["name"]][key] = value
            except KeyError, e:
                #should never execute
                logging.debug("Field " + str(key) + " not found for node " + str(node["name"]))
                logging.debug("Cotinuing...")
                continue

    logging.debug(pformat(chn))

    routing = generate_routing_from_chain(chain)
    
    for node_name, node_rt in routing["routing_table"].items():
        route[node_name] = node_rt

    logging.debug(pformat((route)))
        
    ips = []
    for node_name in chn.keys():
        ips.append(chn[node_name]["address"])
    
    #set config values ---> config(functional_type, (field, value))
    for node in configuration["nodes"]:
        for key, value in node.items():
            #id field is redundant
            if key != "id":
                try:
                    if key == "configuration":
                        #initialize with empty config
                        config[node["id"]][key] = []
                        #make sure configuration refers to a node belonging to the current chain, otherwise skip configuraion field
                        
                        for value_item in value:
                            if isinstance(value_item, dict):
                                for config_item_key, config_item_value in value_item.items():
                                    if config_item_key in ips and config_item_value in ips:
                                        #valid config, add it
                                        config[node["id"]][key].append(value_item)
                            else:
                                if value_item in ips:
                                    config[node["id"]][key].append(value_item)
                    else:
                        config[node["id"]][key] = value
                except KeyError, e:
                    #node not found in current chain 
                    logging.debug("Field " + key + " not found for node " + str(node["id"]))
                    logging.debug(key + " doesn't belong to the current chain, thus it will be skipped!")
                    continue
    
    #debug print of the configuration                
    logging.debug(pformat(config))
        
    #prepare a few more helpful data structures
    nodes_names = []
    nodes_types = []
    nodes_addresses = []
    nodes_ip_mappings = []
    nodes_rt = {}
    
    #initialize vectors for node names and routing tables
    for name in chn.keys():
        nodes_names.append(name)
        nodes_rt[name] = []
    
    #add functional types, addresses and ip mapping to vectors    
    for node, field in chn.items():
        nodes_types.append(field["functional_type"])
        nodes_addresses.append(field["address"])
        nodes_ip_mappings.append(node + ", ctx." + field["address"])        
    
    for node, rt in route.items():
        for dest, next_hop in rt.items():
            #row = "ctx." + dest + ", " + next_hop
            row = "ctx." + dest + ", " + next_hop
            try:
                nodes_rt[node].append(row)
            except KeyError, e:
                #node not found, notify and raise exception
                logging.debug("Node " + node + " not found!")
                raise
                     
    #begin file generation    
    logging.debug("* instantiating chain #" + str(number))
    dirname = os.path.dirname(output_file)
    basename = os.path.basename(output_file)
    basename = os.path.splitext(basename)[0]

    with smart_open(os.path.join(dirname, basename + "_" + str(number) + ".py")) as f:
        c = CodeGeneratorBackend()
        c.begin(tab="    ")
        
        c.writeln("import components")
        
        c.writeln("def " + basename + "_" + str(number) + "():")
        
        c.indent()
        c.write("ctx = components.Context(")
        #write a list of nodes like the following:
        #
        #['a', 'b', 'c']
        c.write_list(nodes_names)
        c.append(",")
        #write a list of ip addresses like the following:
        #
        #['ip_a', 'ip_b', 'ip_c']
        c.write_list(nodes_addresses)
        c.append(")\n")

        c.writeln("net = components.Network (ctx)")

        for i in range(0, len(nodes_names)):
            #write a line like <node_name> = components.<node_class>(ctx.<node_name>, net, ctx):
            #
            #a = components.EndHost(ctx.a, net, ctx)
            #server = components.PolitoWebServer(ctx.server, net, ctx)
            c.write(str(nodes_names[i]) + " = components." + devices_to_classes[str(nodes_types[i])] + "(ctx." + str(nodes_names[i]) + ", net, ctx")
            if devices_initialization[nodes_types[i]] != [] :
                for param in devices_initialization[nodes_types[i]]:
                    c.append(", ctx." + config[nodes_names[i]][param])
            c.append(")\n")
        
        #SET ADDRESS MAPPINGS
        c.write("net.setAddressMappings(")
        #write a list of tuple like [(<node_name>, <node_address>)]:
        #
        #[(a, ctx.ip_a), (server), (ctx.ip_server)]
        c.write_list(nodes_ip_mappings, wrapper="b")
        c.append(")\n")

        #CONFIGURE ROUTING TABLE HERE
        c.writeln("#configure routing table")
        for i in range(0, len(nodes_names)):
            #write a line like:
            #
            #net.RoutingTable(<node_name>, [(ctx.<destination_address1>, <next_hop1>]), (ctx.<destination_address2>, <next_hop2>]))
            c.write("net.RoutingTable(" + str(nodes_names[i]) + ", ")
            c.write_list(nodes_rt[nodes_names[i]], wrapper="b")
            c.append(")\n")
        
        #ATTACH DEVICES
        c.writeln("#attach devices")
        c.write("net.Attach(")
        #write a line like net.Attach(<node1_name>, <node2_name>):
        #
        #net.Attach(a, server, politoCache, fw)
        c.write_list(nodes_names, delimiter = False, wrapper="")
        c.append(")\n")

        #CONFIGURE MIDDLE-BOXES
        c.writeln("#configure middle-boxes")
        for i in range(0, len(nodes_names)):
            #configure middle-box only if its configuration is not empty
            if config[nodes_names[i]]["configuration"] != []:
                #write a line like <node_name>.<node_configuration_method>([(key1, value1), (key2, value2)])
                #
                #fw.AddAcls([(ctx.ip_server, ctx.ip_a)])
                c.write(nodes_names[i] + "." + devices_to_configuration_methods[nodes_types[i]] + "(")
                c.write_list(formatted_list_from_list_of_maps(config[nodes_names[i]]["configuration"]), wrapper="")
                c.append(")\n")

        #CREATE CUSTOM CLASS
        c.writeln("class AutoGeneratedReturn" + str(number) + " (object):")
        
        c.indent()
        c.write("def __init__ (self, net, ctx, ")
        #write a list of nodes like <node1_name, node2_name, node3_name>):
        #
        #a, server, politoCache):
        c.write_list(nodes_names, delimiter = False, wrapper="")
        c.append("):\n")

        c.indent()
        c.writeln("self.net = net")

        c.writeln("self.ctx = ctx")

        for i in range(0, len(nodes_names)):
            #write a line like self.<node_name> = <node_name>
            #
            #self.server = server
            c.writeln("self." + str(nodes_names[i]) + " = " + str(nodes_names[i]))
        
        c.writeln("self.check = components.PropertyChecker (ctx, net)")
        
        c.dedent()
        c.dedent()
        c.write("return AutoGeneratedReturn" + str(number) + "(net, ctx, ")
        #write a list of nodes like <node1_name, node2_name, node3_name>)
        #
        #a, server, politoCache)
        c.write_list(nodes_names, delimiter = False, wrapper="")
        c.append(")\n")

        #write c object to file    
        print >>f, c.end()

        logging.debug("wrote test file " + os.path.abspath(os.path.join(dirname, basename + "_" + str(number)) + ".py") + " successfully!")


def main(argv):
    if len(argv) < 6:
        print 'test_class_generator.py -c <chain_file> -f <conf_file> -o <output_name>'
        sys.exit(2)

    try:
        opts, args = getopt.getopt(argv,"hc:f:r:o:",["help","chain=","config=","route=","ofile="])
    except getopt.GetoptError as err:
        print str(err)
        print 'test_class_generator.py -c <chain_file> -f <conf_file> -o <output_name>'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print 'test_class_generator.py -c <chain_file> -f <conf_file> -o <output_name'
            sys.exit()
        elif opt in ("-c", "--chain"):
            chains_file = arg
        elif opt in ("-f", "--config"):
            configuration_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg

    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    
    #parse chains file
    chains = convert_unicode_to_ascii(parse_json_file(chains_file))
    
    #parse configuration file
    configuration = convert_unicode_to_ascii(parse_json_file(configuration_file))

    logging.debug(pformat((chains)))
    logging.debug(pformat((configuration)))
    
    #custom formatted prints
    #print_chains(chains)
    #print_configuration(configuration)
    #print_routing_table(routing)

    number_of_chains = 0    
    for chain in chains["chains"]:
        number_of_chains += 1;
        generate_test_file(chain, number_of_chains, configuration, output_file)

if __name__ == "__main__":
    main(sys.argv[1:])

    
