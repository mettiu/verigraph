#!/usr/bin/env python

import copy
import sys
from pprint import pprint

from nffg import *



def matteo():
  test = NFFG(id="SG-decomp", name="SG-name")
  sap1 = test.add_sap(name="SAP1", id="sap1")
  sap2 = test.add_sap(name="SAP2", id="sap2")
  comp = test.add_nf(id="comp", name="COMPRESSOR", func_type="customComponent",
                     cpu=1, mem=1, storage=0)
  decomp = test.add_nf(id="decomp", name="DECOMPRESSOR",
                       func_type="headerDecompressor", cpu=1, mem=1, storage=0)
  fwd = test.add_nf(id="fwd", name="FORWARDER", func_type="simpleForwarder",
                    cpu=1, mem=1, storage=0)
  test.add_sglink(sap1.add_port(1), comp.add_port(1), id=1)
  test.add_sglink(comp.ports[1], decomp.add_port(1), id=2)
  test.add_sglink(decomp.ports[1], sap2.add_port(1), id=3)
  test.add_sglink(sap2.ports[1], fwd.add_port(1), id=4)
  test.add_sglink(fwd.ports[1], sap1.ports[1], id=5)

  test.add_req(sap1.ports[1], sap2.ports[1], bandwidth=4, delay=20,
               sg_path=(1, 2, 3))
  test.add_req(sap2.ports[1], sap1.ports[1], bandwidth=4, delay=20,
               sg_path=(4, 5))
  return test


if __name__ == "__main__":
  # test_NFFG()
  # nffg = generate_mn_topo()
  # nffg = generate_mn_test_req()
  # nffg = generate_dynamic_fallback_nffg()
  # nffg = generate_static_fallback_topo()
  # nffg = generate_one_bisbis()
  # nffg = gen()
  # nffg = generate_sdn_topo2()
  # nffg = generate_sdn_req()
  # nffg = generate_os_req()
  # nffg = generate_os_mn_req()
  # nffg = generate_dov()
  # nffg = generate_global_req()
  # nffg = generate_ewsdn_req3()
  # nffg = generate_simple_test_topo()

  #nffg = generate_simple_test_req()
  nffg = matteo()

  # pprint(nffg.network.__dict__)
  # nffg.merge_duplicated_links()
  # pprint(nffg.network.__dict__)
  print nffg.dump()
  # print generate_merged_mapped()
