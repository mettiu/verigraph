import sys,os,os.path
#useful for import mapping
sys.path.append(os.path.expanduser('~/escape-shared/pox/ext/escape/util'))
#useful for from escape import *
sys.path.append(os.path.expanduser('~/escape-shared/pox/ext/'))
#useful for from pox import *
sys.path.append(os.path.expanduser('~/escape-shared/pox'))
#useful for from pox import Catalog
sys.path.append(os.path.expanduser('~/escape-shared/mininet'))
#useful for from pox import premapper
sys.path.append(os.path.expanduser('~/escape-shared/pox/ext/escape/verify'))
from pox.core import core
import mapping
from pprint import pprint
import requests
import json
import re
from premapper import Premapper
from escape.util.mapping import ProcessorError
#from mininet.vnfcatalogiminds import Catalog

class GraphVerify(mapping.AbstractMappingDataProcessor):
    
    def pre_mapping_exec (self, input_graph, resource_graph):
        core.getLogger().info("Verifying graph before mapping...")

        premapper = Premapper(input_graph)

        core.getLogger().info("Grenerating requests for the verify web service...")

        pprint(premapper.nodes)
        

        premapper.add_nodes(ip = "192.168.1.117")

        core.getLogger().info("Getting all the possible chains between source and destination nodes...")

        premapper.get_chains(ip = "192.168.1.117")

        core.getLogger().info("Generating all possible test scenarios...")
        
        premapper.generate_chains_for_tests()


        core.getLogger().info("Generating all the tests...")
        premapper.generate_tests()

        core.getLogger().info("Running tests...")
        result = premapper.run_tests()

        if result == False:
            raise ProcessorError("Pre-mapping verification failed!")
            
        # all tests passed
        return False

    def post_mapping_exec (self, input_graph, resource_graph, result_graph):
        return False
