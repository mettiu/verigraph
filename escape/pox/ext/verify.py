def load_src(name, fpath):
    import os, imp
    p = fpath if os.path.isabs(fpath) \
        else os.path.join(os.path.dirname(__file__), fpath)
    return imp.load_source(name, p)

def launch():
	#load_src("mapping", "/home/mininet/escape/pox/ext/escape/util/mapping.py")
	import sys,os,os.path
	#useful for import mapping
	sys.path.append(os.path.expanduser('~/escape/pox/ext/escape/util'))
	#useful for from escape import *
	sys.path.append(os.path.expanduser('~/escape/pox/ext/'))
	#useful for from pox import *
	sys.path.append(os.path.expanduser('~/escape/pox'))
	from mapping import AbstractMappingDataProcessor
	#import mapping
	
	class MyAbstractMappingDataProcessor(AbstractMappingDataProcessor):
		print "Hello World!"
		def pre_mapping_exec (self, input_graph, resource_graph):
			print "Verifying graph before mapping..."
    		return False

  		def post_mapping_exec (self, input_graph, resource_graph, result_graph):
    		return False

if __name__ == "__main__":
	launch()
