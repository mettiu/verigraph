*config.py* module
==================

Contains manager and handling functions for global ESCAPE configuration.

.. inheritance-diagram::
   escape.util.config.ESCAPEConfig
   :parts: 1

:any:`ESCAPEConfig` is a wrapper class for CONFIG.

Module contents
---------------

.. automodule:: escape.util.config
   :members:
   :private-members:
   :special-members:
   :exclude-members: __dict__,__weakref__,__module__
   :undoc-members:
   :show-inheritance:


