*conversion.py* module
======================

Contains helper classes for conversion between different NF-FG representations.

.. inheritance-diagram::
   escape.util.conversion.Virtualizer3BasedNFFGBuilder
   escape.util.conversion.NFFGConverter
   :parts: 1

:any:`Virtualizer3BasedNFFGBuilder` contains the common function for an NFFG
representation based on virtualizer3.py.

:any:`NFFGConverter` contains conversation logic for different :any:`NFFG`
representations.

Module contents
---------------

.. automodule:: escape.util.conversion
   :members:
   :private-members:
   :special-members:
   :exclude-members: __dict__,__weakref__,__module__
   :undoc-members:
   :show-inheritance:
