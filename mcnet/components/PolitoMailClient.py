from . import NetworkObject
import z3
class PolitoMailClient (NetworkObject):
    """MailClient objects"""
    
    def _init (self, node, network, context):
        self.constraints = list ()
        self.ctx = context
        self.PolitoMailClient = node.z3Node
        self._mailClientRules()
        #network.SaneSend (self)

    @property
    def z3Node (self):
        return self.PolitoMailClient

    def _addConstraints (self, solver):
        print "[MailClient] Installing rules."
        solver.add(self.constraints)

    def _mailClientRules (self):
        n_0 = z3.Const('PolitoMailClient_%s_n_0'%(self.PolitoMailClient), self.ctx.node)
        t_0 = z3.Int('PolitoMailClient_%s_t_0'%(self.PolitoMailClient))
        p_0 = z3.Const('PolitoMailClient_%s_p_0'%(self.PolitoMailClient), self.ctx.packet)
        #self.constraints.append(1 == 2)
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailClient, n_0, p_0, t_0), \
                self.ctx.nodeHasAddr(self.PolitoMailClient, self.ctx.packet.src(p_0)))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailClient, n_0, p_0, t_0), \
                self.ctx.packet.origin(p_0) == self.PolitoMailClient)))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailClient, n_0, p_0, t_0), \
                self.ctx.packet.orig_body(p_0) == self.ctx.packet.body(p_0))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.recv(n_0, self.PolitoMailClient, p_0, t_0), \
                self.ctx.nodeHasAddr(self.PolitoMailClient, self.ctx.packet.dest(p_0)))))
        
        # This client is only able to produce POP3 requests
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailClient, n_0, p_0, t_0), \
                           self.ctx.packet.proto(p_0) == self.ctx.POP3_REQUEST)))
        
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoMailClient, n_0, p_0, t_0), \
                           self.ctx.packet.dest(p_0) == self.ctx.ip_mailServer)))

    @property
    def isEndHost (self):
        return True