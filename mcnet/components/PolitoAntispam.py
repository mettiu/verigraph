'''
Created on Mar 10, 2015

@author: Matteo Virgilio
'''
from . import NetworkObject
import z3

class PolitoAntispam (NetworkObject):
    '''
    Antispam Model
    '''

    def _init(self, node, network, context):
        '''
        Constructor
        '''
        self.constraints = list ()
        self.ctx = context
        self.PolitoAntispam = node.z3Node
        #network.SaneSend (self)
        
    def _addConstraints(self, solver):
        solver.add(self.constraints)
        
    def installAntispam(self, blackList):
        n_0 = z3.Const('%s_n_0'%(self.PolitoAntispam), self.ctx.node)
        n_1 = z3.Const('%s_n_1'%(self.PolitoAntispam), self.ctx.node)
        
        p_0 = z3.Const('%s_p_0'%(self.PolitoAntispam), self.ctx.packet)
        
        t_0 = z3.Int('%s_t_0'%(self.PolitoAntispam))
        t_1 = z3.Int('%s_t_1'%(self.PolitoAntispam))
        
        ef_0 = z3.Int('%s_ef_0'%(self.PolitoAntispam))
        
        self.isInBlacklist = z3.Function('%s_isInBlacklist'%(self.PolitoAntispam), z3.IntSort(), z3.BoolSort()) # f:(emailFrom) -> Bool
        
        blConstraint = []
        if not isinstance(blackList, list):
            blackList = [blackList]
        if(len(blackList) != 0):
            for bl in blackList:
                blConstraint.append(ef_0 == bl)
            self.constraints.append(z3.ForAll([ef_0], z3.If(self.isInBlacklist(ef_0) == z3.Or(blConstraint), True, False)))
        else:
            self.constraints.append(z3.ForAll([ef_0], self.isInBlacklist(ef_0) == False))   # Nothing in the blacklist
        
        constraint = z3.ForAll([n_0, p_0, t_0], 
                            z3.Implies(z3.And(self.ctx.send(self.PolitoAntispam, n_0, p_0, t_0), self.ctx.packet.proto(p_0) == self.ctx.POP3_RESPONSE), 
                                z3.And(z3.Exists([n_1, t_1], 
                                            z3.And(self.ctx.recv(n_1, self.PolitoAntispam, p_0, t_1), t_1 < t_0)),
                                       z3.Not(self.isInBlacklist(self.ctx.packet.emailFrom(p_0))))))
        self.constraints.append(constraint)
         
        constraint2 = z3.ForAll([n_0, p_0, t_0], 
                            z3.Implies(z3.And(self.ctx.send(self.PolitoAntispam, n_0, p_0, t_0), self.ctx.packet.proto(p_0) == self.ctx.POP3_REQUEST), 
                                z3.And(z3.Exists([n_1, t_1], 
                                            z3.And(self.ctx.recv(n_1, self.PolitoAntispam, p_0, t_1),
                                                   t_1 < t_0)))))
        self.constraints.append(constraint2)
        
        constraint3 = z3.ForAll([n_0, p_0, t_0], 
                            z3.Implies(self.ctx.send(self.PolitoAntispam, self.ctx.politoErrFunction.z3Node, p_0, t_0), 
                                z3.And(z3.Exists([n_1, t_1], 
                                            z3.And(self.ctx.recv(n_1, self.PolitoAntispam, p_0, t_1), 
                                                   t_1 < t_0,
                                                   self.ctx.packet.emailFrom(p_0) == 1 )))))
        self.constraints.append(constraint3)
        
        constraint4 = z3.ForAll([n_0, p_0, t_0], 
                            z3.Implies(self.ctx.send(self.PolitoAntispam, n_0, p_0, t_0), 
                                z3.Or(self.ctx.packet.proto(p_0) == self.ctx.POP3_REQUEST, self.ctx.packet.proto(p_0) == self.ctx.POP3_RESPONSE)))
        self.constraints.append(constraint4)
        
        constraint5 = z3.ForAll([n_0, p_0, t_0], 
                            z3.Implies(self.ctx.send(self.PolitoAntispam, n_0, p_0, t_0), 
                                z3.Not(self.ctx.nodeHasAddr(self.PolitoAntispam, self.ctx.packet.src(p_0)))))
        self.constraints.append(constraint5)
        
    @property
    def z3Node (self):
        return self.PolitoAntispam
    
    @property
    def isEndHost (self):
        return False