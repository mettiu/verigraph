'''
Created on Mar 10, 2015

@author: Matteo Virgilio
'''
from . import NetworkObject
import z3

class PolitoCache (NetworkObject):
    '''
    Cache Model (NOT WORKING)
    '''

    def _init(self, node, network, context):
        '''
        Constructor
        '''
        self.constraints = list ()
        self.ctx = context
        self.politoCache = node.z3Node
        #network.SaneSend (self)
        
    def _addConstraints(self, solver):
        solver.add(self.constraints)
        
    def installCache(self, internalNodes):
        n_0 = z3.Const('politoCache_%s_n_0'%(self.politoCache), self.ctx.node)
        n_1 = z3.Const('politoCache_%s_n_1'%(self.politoCache), self.ctx.node)
        n_2 = z3.Const('politoCache_%s_n_2'%(self.politoCache), self.ctx.node)
        
        p_0 = z3.Const('politoCache_%s_p_0'%(self.politoCache), self.ctx.packet)
        p_1 = z3.Const('politoCache_%s_p_1'%(self.politoCache), self.ctx.packet)
        p_2 = z3.Const('politoCache_%s_p_2'%(self.politoCache), self.ctx.packet)
        
        t_0 = z3.Int('politoCache_%s_t_0'%(self.politoCache))
        t_1 = z3.Int('politoCache_%s_t_1'%(self.politoCache))
        t_2 = z3.Int('politoCache_%s_t_2'%(self.politoCache))
        
        a_0 = z3.Const('politoCache_%s_a_0'%(self.politoCache), self.ctx.node)
        
        u_0 = z3.Int('politoCache_%s_u_0'%(self.politoCache))
        
        self.isInternalNode = z3.Function('%s_isInternalNode'%(self.politoCache), self.ctx.node, z3.BoolSort())
        self.isInCache = z3.Function('%s_isInCache'%(self.politoCache), z3.IntSort(), z3.IntSort(), z3.BoolSort()) # f:(url, time) -> Bool
        
        assert len(internalNodes) != 0 # No internal nodes => Should never happen
        
        # Modeling the behavior of the isInternalNode() and isInCache() functions
        internalNodesConstraint = []
        for n in internalNodes:
            internalNodesConstraint.append(a_0 == n.z3Node)
        self.constraints.append(z3.ForAll([a_0], z3.If(self.isInternalNode(a_0) == z3.Or(internalNodesConstraint), True, False)))
        #self.constraints.append(z3.ForAll([a_0], self.isInternalNode(a_0) == z3.Or(internalNodesConstraint)))  
        
#         self.constraints.append(z3.ForAll([u_0, t_0], 
#                                         z3.If(z3.Exists([t_1, t_2, p_1, p_2, n_1, n_2], 
#                                             z3.And(t_1 < t_2,
#                                                    t_1 < t_0,
#                                                    t_2 < t_0,
#                                                    self.ctx.recv(n_1, self.politoCache, p_1, t_1),
#                                                    self.ctx.recv(n_2, self.politoCache, p_2, t_2),
#                                                    self.ctx.packet.proto(p_1) == self.ctx.HTTP_REQUEST,
#                                                    self.ctx.packet.proto(p_2) == self.ctx.HTTP_RESPONSE,
#                                                    self.isInternalNode(n_1),
#                                                    z3.Not(self.isInternalNode(n_2)),
#                                                    self.ctx.packet.url(p_1) == u_0,
#                                                    self.ctx.packet.url(p_2) == u_0)), self.isInCache(u_0, t_0) == True, self.isInCache(u_0, t_0) == False)))
        
        self.constraints.append(z3.ForAll([u_0, t_0], 
                                    z3.Implies(self.isInCache(u_0, t_0), 
                                        z3.Exists([t_1, t_2, p_1, p_2, n_1, n_2], 
                                            z3.And(t_1 < t_2,
                                                   t_1 < t_0,
                                                   t_2 < t_0,
                                                   self.ctx.recv(n_1, self.politoCache, p_1, t_1),
                                                   self.ctx.recv(n_2, self.politoCache, p_2, t_2),
                                                   self.ctx.packet.proto(p_1) == self.ctx.HTTP_REQUEST,
                                                   self.ctx.packet.proto(p_2) == self.ctx.HTTP_RESPONSE,
                                                   self.isInternalNode(n_1),
                                                   z3.Not(self.isInternalNode(n_2)),
                                                   self.ctx.packet.url(p_1) == u_0,
                                                   self.ctx.packet.url(p_2) == u_0)))))

        #Always in cache
#         self.constraints.append(z3.ForAll([u_0, t_0], 
#                             self.isInCache(u_0, t_0) == True))
        
        # Modeling the behavior of the cache
        constraint = z3.ForAll([n_0, p_0, t_0], 
                        z3.Implies(z3.And(self.ctx.send(self.politoCache, n_0, p_0, t_0), z3.Not(self.isInternalNode(n_0))), 
                            z3.And(z3.Exists([t_1, n_1], 
                                        z3.And(t_1 < t_0,
                                               self.isInternalNode(n_1), 
                                               self.ctx.recv(n_1, self.politoCache, p_0, t_1))),
                                   self.ctx.packet.proto(p_0) == self.ctx.HTTP_REQUEST,
                                   z3.Not(self.isInCache(self.ctx.packet.url(p_0), t_0)))))
        self.constraints.append(constraint)
        
        yetAnotherConstraint = z3.ForAll([n_0, p_0, t_0], 
                                    z3.Implies(z3.And(self.ctx.send(self.politoCache, n_0, p_0, t_0), self.isInternalNode(n_0)),
                                        z3.And(z3.Exists([p_1, t_1], 
                                                    z3.And(t_1 < t_0, self.ctx.recv(n_0, self.politoCache, p_1, t_1), 
                                                           self.ctx.packet.proto(p_1) == self.ctx.HTTP_REQUEST,
                                                           self.ctx.packet.url(p_1) == self.ctx.packet.url(p_0))),
                                               self.ctx.packet.proto(p_0) == self.ctx.HTTP_RESPONSE,
                                               self.ctx.packet.src(p_0) == self.ctx.packet.dest(p_1),
                                               self.ctx.packet.dest(p_0) == self.ctx.packet.src(p_1),
                                               self.isInCache(self.ctx.packet.url(p_0), t_0))))
        self.constraints.append(yetAnotherConstraint)
        
    @property
    def z3Node (self):
        return self.politoCache
    
    @property
    def isEndHost (self):
        return False