from . import NetworkObject
import z3
class PolitoWebServer (NetworkObject):
    """WebServer objects"""
    
    def _init (self, node, network, context):
        self.constraints = list ()
        self.ctx = context
        self.node = node.z3Node
        self._webServerRules()

    @property
    def z3Node (self):
        return self.node

    def _addConstraints (self, solver):
        solver.add(self.constraints)

    def _webServerRules (self):
        n_0 = z3.Const('webserver_%s_n_0'%(self.node), self.ctx.node)
        t_0 = z3.Int('webserver_%s_t_0'%(self.node))
        t_1 = z3.Int('webserver_%s_t_1'%(self.node))
        p_0 = z3.Const('webserver_%s_p_0'%(self.node), self.ctx.packet)
        p_1 = z3.Const('webserver_%s_p_1'%(self.node), self.ctx.packet)
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.node, n_0, p_0, t_0), \
                self.ctx.nodeHasAddr(self.node, self.ctx.packet.src(p_0)))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.node, n_0, p_0, t_0), \
                self.ctx.packet.origin(p_0) == self.node)))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.node, n_0, p_0, t_0), \
                self.ctx.packet.orig_body(p_0) == self.ctx.packet.body(p_0))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.recv(n_0, self.node, p_0, t_0), \
                self.ctx.nodeHasAddr(self.node, self.ctx.packet.dest(p_0)))))
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], 
                                    z3.Implies(self.ctx.send(self.node, n_0, p_0, t_0), 
                                        z3.Exists([p_1, t_1], 
                                            z3.And(t_1 < t_0,
                                                   self.ctx.packet.url(p_0) == self.ctx.packet.url(p_1),
                                                   self.ctx.recv(n_0, self.node, p_1, t_1),
                                                    self.ctx.packet.proto(p_0) == self.ctx.HTTP_RESPONSE,
                                                    self.ctx.packet.proto(p_1) == self.ctx.HTTP_REQUEST,
                                                    self.ctx.packet.dest(p_0) == self.ctx.packet.src(p_1),
                                                    self.ctx.packet.src(p_0) == self.ctx.packet.dest(p_1)
                                                   )))))

    @property
    def isEndHost (self):
        return True
