from . import NetworkObject
import z3
class PolitoErrFunction (NetworkObject):
    """Error Function objects"""
    
    def _init (self, node, network, context):
        self.constraints = list ()
        self.ctx = context
        self.PolitoErrFunction = node.z3Node
        self._errFunctionRules()
        #network.SaneSend (self)

    @property
    def z3Node (self):
        return self.PolitoErrFunction

    def _addConstraints (self, solver):
        solver.add(self.constraints)
        print "[ERR FUNC] Installing rules."

    def _errFunctionRules (self):
        n_0 = z3.Const('PolitoErrFunction_%s_n_0'%(self.PolitoErrFunction), self.ctx.node)
        t_0 = z3.Int('PolitoErrFunction_%s_t_0'%(self.PolitoErrFunction))
        p_0 = z3.Const('PolitoErrFunction_%s_p_0'%(self.PolitoErrFunction), self.ctx.packet)
#         self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
#                 z3.Implies(self.ctx.send(self.PolitoErrFunction, n_0, p_0, t_0), \
#                 self.ctx.nodeHasAddr(self.PolitoErrFunction, self.ctx.packet.src(p_0)))))
#         self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
#                 z3.Implies(self.ctx.send(self.PolitoErrFunction, n_0, p_0, t_0), \
#                 self.ctx.packet.origin(p_0) == self.PolitoErrFunction)))
#         self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
#                 z3.Implies(self.ctx.send(self.PolitoErrFunction, n_0, p_0, t_0), \
#                 self.ctx.packet.orig_body(p_0) == self.ctx.packet.body(p_0))))
        # We want the ErrFunction not to send out any packet
        self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
                z3.Implies(self.ctx.send(self.PolitoErrFunction, n_0, p_0, t_0), \
                1 == 2)))
#         self.constraints.append(z3.ForAll([n_0, p_0, t_0], \
#                 z3.Implies(self.ctx.recv(n_0, self.PolitoErrFunction, p_0, t_0), \
#                 self.ctx.nodeHasAddr(self.PolitoErrFunction, self.ctx.packet.dest(p_0)))))

    @property
    def isEndHost (self):
        return True
