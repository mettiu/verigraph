import z3
import time

from examples import PolitoCacheNatFwTest

"""Author: Matteo Virgilio"""
"""
    Cache - Nat - Fw     test
    | HOST_A | ----| CACHE |----- | NAT | ----| FW |----- | HOST_B |
                       |
    | HOST_C | --------
    """
def ResetZ3 ():
    z3._main_ctx = None
    z3.main_ctx()
    
def PrintVector (array):
    i=0
    print "*** Printing vector ***"
    for a in array:
        i+=1
        print "#",i
        print a
    print "*** ", i, " elements printed! ***"

def PrintModel (model):
    for d in model.decls():
        print "%s = %s" % (d.name(), model[d])
        print ""
        
k=0
t=0
for i in range(1,100):
    ResetZ3()
    model = PolitoCacheNatFwTest()
    start_time = time.time()
    ret = model.check.CheckIsolationProperty(model.hostA, model.hostB)
    #print("--- EXECUTION TIME = %s seconds ---" % (time.time() - start_time))
    t = t+(time.time() - start_time)
    k=k+1
    #PrintVector(ret.assertions)
    if ret.result == z3.unsat:
        print("UNSAT")  # Nodes a and b are isolated
    else:
        print("SAT")
        #print "Model -> ", PrintModel(ret.model)
        #model.check.ModelToHTML(sys.stdout, ret)
        #print "Violating packet -> ", ret.violating_packet
        #print "Last hop -> ", ret.last_hop
        #print "Last send_time -> ", ret.last_send_time
        #print "Last recv_time -> ", ret.last_recv_time
print "Mean execution time hostA -> hostB", t/k
    
k=0
t=0
for i in range(1,100):
    ResetZ3()
    model = PolitoCacheNatFwTest()
    start_time = time.time()
    ret = model.check.CheckIsolationProperty(model.hostB, model.hostA)
    #print("--- EXECUTION TIME = %s seconds ---" % (time.time() - start_time))
    t = t+(time.time() - start_time)
    k=k+1
    #PrintVector(ret.assertions)
    if ret.result == z3.unsat:
        print("UNSAT")  # Nodes a and b are isolated
    else:
        print("SAT")
        #print "Model -> ", PrintModel(ret.model)
        #model.check.ModelToHTML(sys.stdout, ret)
        #print "Violating packet -> ", ret.violating_packet
        #print "Last hop -> ", ret.last_hop
        #print "Last send_time -> ", ret.last_send_time
        #print "Last recv_time -> ", ret.last_recv_time
print "Mean execution time hostB -> hostA", t/k