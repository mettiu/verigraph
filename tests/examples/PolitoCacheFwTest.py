import components
def PolitoCacheFwTest ():
    """
    Custom test
        | A | <------> | CACHE | <------> | FW | <------> | PolitoWebServer |
    
    """
    ctx = components.Context (['a', 'server', 'politoCache', 'fw'],\
                              ['ip_a', 'ip_server', 'ip_cache', 'ip_fw'])
    net = components.Network (ctx)
    a = components.EndHost(ctx.a, net, ctx) 
    server = components.PolitoWebServer(ctx.server, net, ctx) 
    politoCache = components.PolitoCache(ctx.politoCache, net, ctx)
    fw = components.AclFirewall(ctx.fw, net, ctx)
    net.setAddressMappings([(a, ctx.ip_a), \
                            (server, ctx.ip_server), \
                            (politoCache, ctx.ip_cache), \
                            (fw, ctx.ip_fw)])
    
    net.RoutingTable(a, [(ctx.ip_server, politoCache)])
    net.RoutingTable(server, [(ctx.ip_a, fw)])

    net.RoutingTable(politoCache, [(ctx.ip_a, a), (ctx.ip_server, fw)])
    net.RoutingTable(fw, [(ctx.ip_a, politoCache), (ctx.ip_server, server)])
    net.Attach(a, server, politoCache, fw)
    
    # Configuring middleboxes
    politoCache.installCache([ctx.a])
    fw.AddAcls([(ctx.ip_server, ctx.ip_a)])
    
    class PolitoCacheFwReturn (object):
        def __init__ (self, net, ctx, a, server, politoCache):
            self.net = net
            self.ctx = ctx
            self.a = a
            self.server = server
            self.politoCache = politoCache
            self.check = components.PropertyChecker (ctx, net)
    return PolitoCacheFwReturn(net, ctx, a, server, politoCache) 
