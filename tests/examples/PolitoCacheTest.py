import components
def PolitoCacheTest ():
    """Custom test"""
    ctx = components.Context (['a', 'b', 'politoCache'],\
                              ['ip_a', 'ip_b', 'ip_cache'])
    net = components.Network (ctx)
    a = components.EndHost(ctx.a, net, ctx) 
    b = components.EndHost(ctx.b, net, ctx) 
    politoCache = components.PolitoCache(ctx.politoCache, net, ctx)
    net.setAddressMappings([(a, ctx.ip_a), \
                            (b, ctx.ip_b), \
                            (politoCache, ctx.ip_cache)])
    
    addresses = [ctx.ip_a, ctx.ip_b]
    net.RoutingTable(a, [(x, politoCache) for x in addresses])
    net.RoutingTable(b, [(x, politoCache) for x in addresses])

    net.RoutingTable(politoCache, [(ctx.ip_a, a), (ctx.ip_b, b)])
    net.Attach(a, b, politoCache)
    politoCache.installCache([ctx.a])
    
    class PolitoCacheReturn (object):
        def __init__ (self, net, ctx, a, b, politoCache):
            self.net = net
            self.ctx = ctx
            self.a = a
            self.b = b
            self.politoCache = politoCache
            self.check = components.PropertyChecker (ctx, net)
    return PolitoCacheReturn(net, ctx, a, b, politoCache) 
