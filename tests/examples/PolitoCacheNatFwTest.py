import components
def PolitoCacheNatFwTest ():
    """
    Cache - Nat - Fw     test
    | HOST_A | ----| CACHE |----- | NAT | ----| FW |----- | HOST_B |
                       |
    | HOST_C | --------
    """
    # Context setup
    ctx = components.Context (['hostA', 'hostC', 'politoCache', 'politoNat', 'politoFw', 'hostB'],\
                              ['ip_hostA', 'ip_hostC', 'ip_politoCache', 'ip_politoNat', 'ip_politoFw', 'ip_hostB'])
    net = components.Network (ctx)
    #hostA = components.PolitoWebClient(ctx.hostA, net, ctx, ctx.ip_hostC) # Last argument is the server ip address
    hostA = components.PolitoWebClient(ctx.hostA, net, ctx, ctx.ip_hostB)
    hostB = components.PolitoWebServer(ctx.hostB, net, ctx)
    hostC = components.PolitoWebServer(ctx.hostC, net, ctx) 
    politoCache = components.PolitoCache(ctx.politoCache, net, ctx) 
    politoNat = components.PolitoNat(ctx.politoNat, net, ctx)
    politoFw = components.AclFirewall(ctx.politoFw, net, ctx)
    net.setAddressMappings([(hostA, ctx.ip_hostA), \
                            (hostB, ctx.ip_hostB), \
                            (hostC, ctx.ip_hostC), \
                            (politoCache, ctx.ip_politoCache), \
                            (politoNat, ctx.ip_politoNat), \
                            (politoFw, ctx.ip_politoFw)])
    
    # Configuring middleboxes routing tables
    net.RoutingTable(politoFw, [(ctx.ip_hostB, hostB), 
                             (ctx.ip_hostA, politoNat),
                             (ctx.ip_hostC, politoNat),
                             (ctx.ip_politoNat, politoNat),
                             (ctx.ip_politoCache, politoNat)])
    
    net.RoutingTable(politoNat, [(ctx.ip_hostA, politoCache), 
                             (ctx.ip_hostB, politoFw),
                             (ctx.ip_hostC, politoCache),
                             (ctx.ip_politoCache, politoCache),
                             (ctx.ip_politoFw, politoFw)])
    
    net.RoutingTable(politoCache, [(ctx.ip_hostA, hostA), 
                             (ctx.ip_hostB, politoNat),
                             (ctx.ip_hostC, hostC),
                             (ctx.ip_politoNat, politoNat),
                             (ctx.ip_politoFw, politoNat)])
    
    net.RoutingTable(hostA, [(ctx.ip_hostB, politoCache), 
                             (ctx.ip_hostC, politoCache),
                             (ctx.ip_politoCache, politoCache),
                             (ctx.ip_politoNat, politoCache),
                             (ctx.ip_politoFw, politoCache)])
    
    net.RoutingTable(hostC, [(ctx.ip_hostB, politoCache),
                             (ctx.ip_hostA, politoCache), 
                             (ctx.ip_politoCache, politoCache),
                             (ctx.ip_politoNat, politoCache),
                             (ctx.ip_politoFw, politoCache)])
    
    net.RoutingTable(hostB, [(ctx.ip_hostA, politoFw), 
                             (ctx.ip_hostC, politoFw),
                             (ctx.ip_politoCache, politoFw),
                             (ctx.ip_politoNat, politoFw),
                             (ctx.ip_politoFw, politoFw)])

    # Attaching nodes to the network
    net.Attach(hostA, hostB, hostC, politoCache, politoNat, politoFw)
    
    # Configuring middleboxes
    politoNat.setInternalAddress([ctx.ip_hostA, ctx.ip_hostC])
    politoCache.installCache([hostA])
    politoFw.AddAcls([(ctx.ip_politoNat, ctx.ip_hostB)])
    
    class PolitoCacheNatFwTestReturn (object):
        def __init__ (self, net, ctx, hostA, hostB, hostC, politoCache, politoNat, politoFw):
            self.net = net
            self.ctx = ctx
            self.hostA = hostA
            self.hostB = hostB
            self.hostC = hostC
            self.politoCache = politoCache
            self.politoNat = politoNat
            self.politoFw = politoFw
            self.check = components.PropertyChecker (ctx, net)
    return PolitoCacheNatFwTestReturn(net, ctx, hostA, hostB, hostC, politoCache, politoNat, politoFw) 
