import components
def CustomPolitoTest ():
    """Custom test"""
    ctx = components.Context (['a', 'b', 'c', 'politoNF'],\
                              ['ip_a', 'ip_b', 'ip_c', 'ip_f'])
    net = components.Network (ctx)
    a = components.EndHost(ctx.a, net, ctx) 
    b = components.EndHost(ctx.b, net, ctx) 
    c = components.EndHost(ctx.c, net, ctx) 
    politoNF = components.PolitoNF(ctx.politoNF, net, ctx)
    net.setAddressMappings([(a, ctx.ip_a), \
                            (b, ctx.ip_b), \
                            (c, ctx.ip_c), \
                            (politoNF, ctx.ip_f)])
    addresses = [ctx.ip_a, ctx.ip_b, ctx.ip_c, ctx.ip_f]
    net.RoutingTable(a, [(x, politoNF) for x in addresses])
    net.RoutingTable(b, [(x, politoNF) for x in addresses])
    net.RoutingTable(c, [(x, politoNF) for x in addresses])

    #net.SetGateway(a, fw)
    #net.SetGateway(b, fw)
    #net.SetGateway(c, fw)

    net.RoutingTable(politoNF, [(ctx.ip_a, a), \
                          (ctx.ip_b, b), \
                          (ctx.ip_c, c)])
    #fw.AddAcls([(ctx.ip_a, ctx.ip_c)])
    politoNF.politoNFRules(ctx.ip_a, ctx.ip_b)
    net.Attach(a, b, c, politoNF)
    #endhosts = [a, b, c]
    class CustomPolitoReturn (object):
        def __init__ (self, net, ctx, a, b, c, fw):
            self.net = net
            self.ctx = ctx
            self.a = a
            self.b = b
            self.c = c
            self.politoNF = politoNF
            self.check = components.PropertyChecker (ctx, net)
    return CustomPolitoReturn(net, ctx, a, b, c, politoNF) 
