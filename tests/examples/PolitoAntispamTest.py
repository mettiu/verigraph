import components
def PolitoAntispamTest ():
    """
    Antispam test
    | CLIENT | --------- | ANTISPAM | --------- | MAIL SERVER |
                               |
                               |
                               |
                        | ERR FUNCTION |
    """
    ctx = components.Context (['politoMailClient', 'politoAntispam', 'politoMailServer', 'politoErrFunction'],\
                              ['ip_client', 'ip_antispam', 'ip_mailServer', 'ip_errFunction'])
    net = components.Network (ctx)
    politoMailClient = components.PolitoMailClient(ctx.politoMailClient, net, ctx) 
    politoAntispam = components.PolitoAntispam(ctx.politoAntispam, net, ctx) 
    politoMailServer = components.PolitoMailServer(ctx.politoMailServer, net, ctx)
    politoErrFunction = components.PolitoErrFunction(ctx.politoErrFunction, net, ctx)
    net.setAddressMappings([(politoMailClient, ctx.ip_client), \
                            (politoAntispam, ctx.ip_antispam), \
                            (politoMailServer, ctx.ip_mailServer), \
                            (politoErrFunction, ctx.ip_errFunction)])
    
    net.RoutingTable(politoMailClient, [(ctx.ip_mailServer, politoAntispam), (ctx.ip_errFunction, politoErrFunction)])
     
    net.RoutingTable(politoAntispam, [(ctx.ip_mailServer, politoMailServer), (ctx.ip_client, politoMailClient), (ctx.ip_errFunction, politoErrFunction)])
    #net.RoutingTable(politoAntispam, [(ctx.ip_errFunction, politoErrFunction)])
     
    net.RoutingTable(politoMailServer, [(ctx.ip_client, politoAntispam)])

    net.Attach(politoMailClient, politoAntispam, politoMailServer, politoErrFunction)
    politoAntispam.installAntispam([1])
    
    class PolitoAntispamTestReturn (object):
        def __init__ (self, net, ctx, politoMailClient, politoAntispam, politoMailServer, politoErrFunction):
            self.net = net
            self.ctx = ctx
            self.politoMailClient = politoMailClient
            self.politoAntispam = politoAntispam
            self.politoMailServer = politoMailServer
            self.politoErrFunction = politoErrFunction
            self.check = components.PropertyChecker (ctx, net)
    return PolitoAntispamTestReturn(net, ctx, politoMailClient, politoAntispam, politoMailServer, politoErrFunction) 
