import components
def PolitoDoubleFwTest ():
    """Custom test"""
    ctx = components.Context (['a', 'b', 'c', 'fw1', 'fw2'],\
                              ['ip_a', 'ip_b', 'ip_c', 'ip_fw1', 'ip_fw2'])
    net = components.Network (ctx)
    a = components.EndHost(ctx.a, net, ctx) 
    b = components.EndHost(ctx.b, net, ctx) 
    c = components.EndHost(ctx.c, net, ctx) 
    fw1 = components.PolitoNF(ctx.fw1, net, ctx)
    fw2 = components.PolitoNF(ctx.fw2, net, ctx)
    net.setAddressMappings([(a, ctx.ip_a), \
                            (b, ctx.ip_b), \
                            (c, ctx.ip_c), \
                            (fw1, ctx.ip_fw1), \
                            (fw2, ctx.ip_fw2)])
    addresses = [ctx.ip_a, ctx.ip_b, ctx.ip_c]
    net.RoutingTable(a, [(x, fw1) for x in addresses])
    net.RoutingTable(b, [(x, fw1) for x in addresses])
    net.RoutingTable(c, [(x, fw2) for x in addresses])

    #net.SetGateway(a, nat)
    #net.SetGateway(b, nat)
    #net.SetGateway(c, fw)

    net.RoutingTable(fw1, [(ctx.ip_a, a), \
                          (ctx.ip_b, b), \
                          (ctx.ip_c, fw2)])
    net.RoutingTable(fw2, [(ctx.ip_a, fw1), \
                                (ctx.ip_b, fw1), \
                                (ctx.ip_c, c)])
    net.Attach(a, b, c, fw1, fw2)
    fw1.politoNFRules(ctx.ip_a, ctx.ip_c)  # We install the rule on fw1
    fw2.politoNFRules(ctx.ip_b, ctx.ip_c)  # We install the rule on fw2
    class PolitoDoubleFwReturn (object):
        def __init__ (self, net, ctx, a, b, c, fw1, fw2):
            self.net = net
            self.ctx = ctx
            self.a = a
            self.b = b
            self.c = c
            self.fw1 = fw1
            self.fw2 = fw2
            self.check = components.PropertyChecker (ctx, net)
    return PolitoDoubleFwReturn(net, ctx, a, b, c, fw1, fw2) 
