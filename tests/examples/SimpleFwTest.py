import components
def SimpleFwTest ():
    """Simple ACL firewall test"""
    ctx = components.Context (['a', 'b', 'c', 'fw'],\
                              ['ip_a', 'ip_b', 'ip_c', 'ip_f'])
    net = components.Network (ctx)
    a = components.EndHost(ctx.a, net, ctx) 
    b = components.EndHost(ctx.b, net, ctx) 
    c = components.EndHost(ctx.c, net, ctx) 
    fw = components.AclFirewall(ctx.fw, net, ctx)
    net.setAddressMappings([(a, ctx.ip_a), \
                            (b, ctx.ip_b), \
                            (c, ctx.ip_c), \
                            (fw, ctx.ip_f)])
    addresses = [ctx.ip_a, ctx.ip_b, ctx.ip_c, ctx.ip_f]
    net.RoutingTable(a, [(x, fw) for x in addresses])
    net.RoutingTable(b, [(x, fw) for x in addresses])
    net.RoutingTable(c, [(x, fw) for x in addresses])

    #net.SetGateway(a, fw)
    #net.SetGateway(b, fw)
    #net.SetGateway(c, fw)

    net.RoutingTable(fw, [(ctx.ip_a, a), \
                          (ctx.ip_b, b), \
                          (ctx.ip_c, c)])
    fw.AddAcls([(ctx.ip_a, ctx.ip_c), (ctx.ip_a, ctx.ip_b), (ctx.ip_b, ctx.ip_c)])
    net.Attach(a, b, c, fw)
    #endhosts = [a, b, c]
    class SimpleFwReturn (object):
        def __init__ (self, net, ctx, a, b, c, fw):
            self.net = net
            self.ctx = ctx
            self.a = a
            self.b = b
            self.c = c
            self.fw = fw
            self.check = components.PropertyChecker (ctx, net)
    return SimpleFwReturn(net, ctx, a, b, c, fw) 
