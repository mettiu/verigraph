import z3

from examples import PolitoCacheTest

"""Author: Matteo Virgilio"""
def ResetZ3 ():
    z3._main_ctx = None
    z3.main_ctx()
    
def PrintVector (array):
    i=0
    print "*** Printing vector ***"
    for a in array:
        i+=1
        print "#",i
        print a
    print "*** ", i, " elements printed! ***"

def PrintModel (model):
    for d in model.decls():
        print "%s = %s" % (d.name(), model[d])
        print ""
        
ResetZ3()
model = PolitoCacheTest()
ret = model.check.CheckIsolationProperty(model.a, model.b)
PrintVector(ret.assertions)
if ret.result == z3.unsat:
    print("UNSAT")  # Nodes a and b are isolated
else:
    print("SAT")
    #print "Model -> ", PrintModel(ret.model)
    #model.check.ModelToHTML(sys.stdout, ret)
    #print "Violating packet -> ", ret.violating_packet
    #print "Last hop -> ", ret.last_hop
    #print "Last send_time -> ", ret.last_send_time
    #print "Last recv_time -> ", ret.last_recv_time