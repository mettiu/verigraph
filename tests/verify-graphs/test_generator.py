#!/usr/bin/python

from pprint import pprint
from code_generator import CodeGeneratorBackend
import sys, getopt
import contextlib
import os
from utility import *

def main(argv):
    if len(argv) < 8:
        print 'test_generator.py -i <inputfile> -o <outputfile> -s <source> -d <destination>'
        sys.exit(2)
    inputfile = ''
    outputfile = ''
    source = ''
    destination = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:s:d:",["ifile=","ofile=","source=","destination="])
    except getopt.GetoptError:
        print 'test_generator.py -i <inputfile> -o <outputfile> -s <source> -d <destination>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'test_generator.py -i <inputfile> -o <outputfile> -s <source> -d <destination>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-s", "--source"):
            source = arg
        elif opt in ("-d", "--destination"):
            destination = arg
        
    print 'Input file is', inputfile
    print 'Output file is', outputfile
    print 'Source node is', source
    print 'Destination node is', destination
    
    with smart_open(outputfile) as f:
        c = CodeGeneratorBackend()
        c.begin(tab="    ")
        
        c.writeln("import z3")

        c.write("from ")
        c.append(os.path.splitext(inputfile)[0])
        c.append(" import ")
        c.append(os.path.splitext(inputfile)[0] + "\n")

        c.writeln("def ResetZ3():")

        c.indent()
        c.writeln("z3._main_ctx = None")

        c.writeln("z3.main_ctx()")

        c.dedent()
        c.writeln("def PrintVector (array):")

        c.indent()
        c.writeln("i=0")

        c.writeln("print \"*** Printing vector ***\"")

        c.writeln("for a in array:")

        c.indent()
        c.writeln("i+=1")

        c.writeln("print \"#\",i")
        
        c.writeln("print a")

        c.dedent()
        c.writeln("print \"*** \", i, \" elements printed! ***\"")

        c.dedent()
        c.writeln("def PrintModel (model):")

        c.indent()
        c.writeln("for d in model.decls():")

        c.indent()
        c.writeln("print \"%s = %s\" % (d.name(), model[d])")

        c.writeln("print \"\"")

        c.dedent()
        c.dedent()
        
        c.writeln("ResetZ3()")

        c.write("model = ")
        c.append(os.path.splitext(inputfile)[0] + "()\n")

        c.write("ret = model.check.CheckIsolationProperty(model.")
        c.append(source + ", model." + destination + ")\n")

        c.writeln("PrintVector(ret.assertions)")

        c.writeln("if ret.result == z3.unsat:")

        c.indent()
        c.writeln("print(\"UNSAT\")")

        c.dedent()
        c.writeln("else:")

        c.indent()
        c.writeln("print(\"SAT\")")
        c.dedent()

        print >>f, c.end()
    print "File " + outputfile + " has been successfully generated!"       

if __name__ == "__main__":
    main(sys.argv[1:])
