#!/usr/bin/python

from lib import test_class_generator
from lib import test_generator
from lib import code_generator
from lib import json_generator
from lib import utility
import subprocess
import os
import sys
from pprint import pprint

def main():
    first_execution = True
    curr_dir = os.path.abspath(".")
    
    chains_file = "chains.json"
    config_file = "config.json"
    test_class_file = "something"
    test_file = "something"
    
    
    while True:
        choice = raw_input("""CHAIN TEST\n
        Choose one of the following options:\n
        1) Generate json files
        2) Generate custom test class
        3) Generate custom test from test class
        4) Run test and quit
        5) quit\n-->""")
        
        
        try:
            if int(choice) == 1:
                #generate json files
                chains_file, config_file, routing_file = json_generator.main()
                print chains_file, config_file, routing_file

            elif int(choice) == 2:
                #generate test class
                wd = raw_input("Working directory is " + curr_dir + ", correct? (Y/N)")
                if wd == "N" or wd == "n":
                    utility.list_directories(curr_dir)
                    curr_dir = os.path.abspath(raw_input("Working directory? -->"))
                utility.list_files(curr_dir)
                chains_json = raw_input("Chains file? (default is \"" + chains_file + "\") -->")
                if chains_json != "":
                    chains_file = chains_json
                configuration_json = raw_input("Configuration file? (default is \"" + config_file + "\") -->")
                if configuration_json != "":
                    config_file = configuration_json
                output_file = raw_input("Output file? -->")
                arguments = ["-c", curr_dir + "/" + chains_file, "-f", curr_dir + "/" + config_file, "-o", curr_dir + "/" + output_file]
                test_class_generator.main(arguments)
            elif int(choice) == 3:
                #generate custom test from test class
                wd = raw_input("Working directory is " + curr_dir + ", correct? (Y/N)")
                if wd == "N" or wd == "n":
                    curr_dir = raw_input("Working directory? -->")
                input_file = raw_input("Input file? -->")
                output_file = raw_input("Output file? -->")
                source = raw_input("Source node? -->")
                destination = raw_input("Destination node? -->")
                #command = str(os.path.abspath(".") + "/lib/test_generator.py" + " -i " + curr_dir + "/" + input_file + " -o " + curr_dir + "/" + output_file + " -s " + curr_dir + "/" + source + " -d " + curr_dir + "/" + destination)
                arguments = ["-i", curr_dir + "/" + input_file, "-o", curr_dir + "/" + output_file, "-s", source, "-d", destination]
                test_generator.main(arguments)
            elif int(choice) == 4:
                #run test)
                wd = raw_input("Working directory is " + curr_dir + ", correct? (Y/N)")
                if wd == "N" or wd == "n":
                    curr_dir = raw_input("Working directory? -->")
                test_file = raw_input("Test file name? -->")
                command = str(curr_dir + "/" + test_file)
                subprocess.call(["python", command])
                break
            elif int(choice) == 5:
                #quit
                sys.exit(0)
            else:
                print("Invalid choice, please try again!")
        except ValueError, e:
            print "Invalid choice, please try again!"
            continue

if __name__ == "__main__":
    main()
