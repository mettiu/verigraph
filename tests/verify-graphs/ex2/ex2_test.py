import z3
from ex2_1 import ex2_1
def ResetZ3():
    z3._main_ctx = None
    z3.main_ctx()
def PrintVector (array):
    i=0
    print "*** Printing vector ***"
    for a in array:
        i+=1
        print "#",i
        print a
    print "*** ", i, " elements printed! ***"
def PrintModel (model):
    for d in model.decls():
        print "%s = %s" % (d.name(), model[d])
        print ""
ResetZ3()
model = ex2_1()
ret = model.check.CheckIsolationProperty(model.hostA, model.hostB)
PrintVector(ret.assertions)
if ret.result == z3.unsat:
    print("UNSAT")
else:
    print("SAT")

